/*
 * Copyright 2018 Leo Sutic <leo@sutic.nu>. All Rights Reserved.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
function Imception () {
}

Imception.APPNAME = "nu.sutic.imception";
Imception.TF_SERVER = "tf_server.exe";

Imception.prototype = {
    
    getSelectedModel : function () {
        return $("#cb-model")[0].value;
    },
    
    setSelectedModel : function (id) {
        $("#cb-model")[0].value = id;
    },
    
    /*
    --------------------------------------------------------------------
    Label Module
    --------------------------------------------------------------------
    */    
    
    showLabelModule : function () {
        this.showModule("label");
        var modelDir = this.getSelectedModel ();
        var model = this.models[modelDir];
        
        $("#btn-label-single, #btn-label-selected").prop("disabled", model == null);
        if (model != null) {            
            $("#label-modelinfo")[0].innerHTML = "<b>" + model.model_name + "</b> has an accuracy of <b>" + Math.round(model.accuracy * 100) + "%</b>, tested on <b>" + model.accuracy_N + "</b> images.";
            return IMWS.get('v1/files',{
                    idlist: IMatch.idlist.fileWindowFocusedFile,
                    fields: 'id,filename'
                })
            .then(function(response) {
                    if (response.files.length > 0) {
                        $("#label-image")[0].src = "/v1/files/image?auth_token=&imagesize=SMALL&id=" + response.files[0].id;
                        $("#label-labels")[0].innerHTML = "<p>Press <b>Preview</b> to detect labels for this image.</p>";
                    } else {
                        $("#label-labels")[0].innerHTML = "<p>Select a file to start labeling.</p>";
                    }
                })
        } else {            
            $("#label-modelinfo")[0].innerHTML = "No model selected.";
            $("#label-labels")[0].innerHTML = "<p>Create a model to start labeling.</p>";
        }
    },
    
    labelSingle : function () {
        var that = this;
        var modelDir = this.getSelectedModel ();
        var model = this.models[modelDir];
        
        return IMWS.get('v1/files',{
                idlist: IMatch.idlist.fileWindowFocusedFile,
                fields: 'id,filename'
            })
        .then(function(response) {
                $("#label-image")[0].src = "/v1/files/image?auth_token=&imagesize=SMALL&id=" + response.files[0].id;
                return that.runLabel(modelDir, IMatch.idlist.fileWindowFocusedFile, 0.0)
            })
        .then (function(response) {
                var labels = JSON.parse(response.data);
                var allLabels = "";
                var allCategories = {};
                
                for (var id in labels) {
                    var categories = labels[id];
                    for (var k in categories) {
                        allCategories[k] = true;
                    }
                }
                
                var substitutions = that.getCategoryNameSubstitutionsForCategories(model, Object.keys(allCategories), [], [
                        that.makeSubstitutions(model.normalizingSubstitutions != null ? model.normalizingSubstitutions : []),
                        that.makeSubstitutions(model.reviewSubstitutions != null ? model.reviewSubstitutions : []),
                        that.makeSubstitutions(model.approvalSubstitutions != null ? model.approvalSubstitutions : [])
                    ]);
                
                
                for (var id in labels) {
                    var categories = labels[id];
                    for (var k in categories) {
                        var assigned = categories[k] > that.models[modelDir].confidenceThreshold;
                        var approvedLabel = substitutions[k];
                        allLabels += "<button type=\"button\" style=\"margin:8px;\" class=\"btn btn-default btn-sm btn-category" + (assigned ? " btn-success" : "") + "\"><span>" + approvedLabel + " (" + (Math.round(categories[k] * 100)) + "%)</span></button>";
                    }                    
                }
                
                $("#label-labels")[0].innerHTML = allLabels;
            });
    },
    
    labelSelected : function () {
        var modelDir = this.getSelectedModel ();
        var model = this.models[modelDir];
        var reviewCategory = model.reviewCategory.trim();
        if (reviewCategory == "") {
            reviewCategory = null;
        }
        var that = this;
        
        var assignments = {
        };
        
        return that.runLabel(modelDir, IMatch.idlist.fileWindowSelection, model.confidenceThreshold)
        .then (function(response) {
                var labels = JSON.parse(response.data);
                
                if (reviewCategory != null) {
                    assignments[reviewCategory] = [];
                }
                
                var allCategories = {};
                
                for (var id in labels) {
                    var categories = labels[id];
                    for (var k in categories) {
                        allCategories[k] = true;
                    }
                }
                
                var substitutions = that.getCategoryNameSubstitutionsForCategories(model, Object.keys(allCategories), [], [
                        that.makeSubstitutions(model.normalizingSubstitutions != null ? model.normalizingSubstitutions : []),
                        that.makeSubstitutions(model.reviewSubstitutions != null ? model.reviewSubstitutions : [])
                    ]);
                
                for (var id in labels) {
                    var categories = labels[id];
                    var anyAssigned = false;
                    for (var k in categories) {
                        var assignedCategory = substitutions[k];
                        if (assignedCategory != null) {
                            if (assignments[assignedCategory] == null) {
                                assignments[assignedCategory] = [];
                            }
                            assignments[assignedCategory].push(id);
                            anyAssigned = true;
                        }
                    }
                    if (anyAssigned && reviewCategory != null) {
                        assignments[reviewCategory].push(id);
                    }
                }
                
                var allNames = "";
                for (var k in assignments) {
                    if (allNames.length > 0) {
                        allNames = allNames + ",";
                    }
                    allNames = allNames + k;
                }
                return IMWS.post('v1/categories/create',{
                        parentid: 0,
                        name: allNames
                    });
            })
        .then (function () {
                var assignmentCalls = [];
                for (var k in assignments) {
                    if (assignments[k].length > 0) {
                        assignmentCalls.push (
                            IMWS.post('v1/categories/assign',{
                                    path: k,
                                    fileid: assignments[k].join(",")
                                })
                        );
                    }
                }
                return Promise.all(assignmentCalls);
            });
    },
    
    runLabel : function (modelDir, idList, minConfidence) {
        var that = this;
        
        var requestFileName = that.config.tempDir + "\\" + Imception.APPNAME + ".json";
        var responseFileName = that.config.tempDir + "\\" + Imception.APPNAME + ".result.json";
        var request = {
            baseUrl: IMatch.DEFAULT_URL,
            "bin_dir": that.config.binDir,
            "work_dir": that.config.tempDir,
            "user_data_dir": that.config.userData,
            "model_dir": modelDir,
            "auth_token": that.authToken,
            "job_control": that.jobControl,
            "flags": {
            },
            "output_file": responseFileName,
            "min_confidence": minConfidence,
            "flags": {},
            "images": []
        };
        
        return IMWS.get('v1/files',{
                idlist: idList,
                fields: 'id,filename'
            })
        .then(function(response) {
                response.files.forEach(function(f) {
                        request.images.push(f.id);
                    });
                return IMWS.post("v1/filesystem/file/writetext", {
                        filename: requestFileName,
                        data: JSON.stringify(request)
                    });
            })
        .then (function (response) {
                return that.resetJobControl();
            })
        .then (function (response) {
                return IMatch.shellExecute({
                        executable : Imception.TF_SERVER,
                        verb : "open",
                        directory: that.config.binDir,
                        parameters : "label_image " + requestFileName,
                        showwindow : that.debugMode
                    })
                .then (function (response) {
                        if (response.result == "ok") {
                            return that.startProgress ("Labeling images using " + that.models[modelDir].model_name);
                        } else {
                            console.log(response);
                            new IMatchModals().showErrorBox({
                                    headline: 'Unable to run TensorFlow',
                                    content: "The TensorFlow executable did not complete successfully. The error was: " + JSON.stringify(response)
                                });
                            throw new Exception ();
                        }
                    }, function () {
                    });
            })
        .then (function(response) {
                if (response.error != null && response.error == true) {
                    var m = new IMatchModals();
                    m.showErrorBox({
                            headline: "An error occurred when labeling images using " + modelName,
                            content: response.message
                        });
                    throw new Exception();
                } else {
                    return IMWS.get("v1/filesystem/file/readtext", {
                            filename: responseFileName
                        });
                }
            });        
    },
    
    /*
    --------------------------------------------------------------------
    Models Module
    --------------------------------------------------------------------
    */
    
    showModelsModule : function () {
        this.showModule("models");
        var model = this.models[this.getSelectedModel()];
        $("#btn-edit-model-train, #btn-edit-model-save").prop("disabled", model == null);
        if (model != null) {
            $("#edit-model-id")[0].value = model.model_dir_name;
            $("#edit-model-id")[0].disabled = true;
            $("#edit-model-name")[0].value = model.model_name;
            $("#edit-model-architecture")[0].value = model.architecture;
            $("#edit-model-training-categories")[0].value = model.trainingCategories != null ? model.trainingCategories.join(";\n") : "";
            $("#edit-model-num-images")[0].value = model.numTrainingImages != null ? model.numTrainingImages : "1500";
            $("#edit-model-num-iterations")[0].value = model.numIterations != null ? model.numIterations : "4000";
            $("#edit-model-confidence-threshold")[0].value = model.confidenceThreshold != null ? model.confidenceThreshold : "0.5";
            $("#edit-model-normalizing-substitutions")[0].value = model.normalizingSubstitutions != null ? model.normalizingSubstitutions.join(";\n") : "";
            $("#edit-model-review-substitutions")[0].value = model.reviewSubstitutions != null ? model.reviewSubstitutions.join(";\n") : "";
            $("#edit-model-review-category")[0].value = model.reviewCategory != null ? model.reviewCategory : "";
            $("#edit-model-approval-substitutions")[0].value = model.approvalSubstitutions != null ? model.approvalSubstitutions.join(";\n") : "";
        }
        return Promise.resolve(null);
    },
    
    saveSelectedModel : function () {
        var model = this.models[this.getSelectedModel()];
        if (model != null) {
            var nti = parseInt($("#edit-model-num-images")[0].value);
            if (isNaN(nti)) {
                alert("Number of images to use in training must be a valid number.");
                return Promise.reject(null);
            }
            var ni = parseInt($("#edit-model-num-iterations")[0].value);
            if (isNaN(ni)) {
                alert("Number of iterations to use in training must be a valid number.");
                return Promise.reject(null);
            }
            var ct = parseFloat($("#edit-model-confidence-threshold")[0].value);
            if (isNaN(ct)) {
                alert("Confidence threshold must be a valid number.");
                return Promise.reject(null);
            }
            var splitField = function (value) {
                return value.split(";").map((s) => s.trim()).filter(s => s.length > 0);
            }
            
            model.model_name = $("#edit-model-name")[0].value;
            model.trainingCategories = splitField($("#edit-model-training-categories")[0].value);
            model.numTrainingImages = nti;
            model.numIterations = ni;
            model.confidenceThreshold = ct;
            
            model.normalizingSubstitutions = splitField($("#edit-model-normalizing-substitutions")[0].value);
            model.reviewSubstitutions = splitField($("#edit-model-review-substitutions")[0].value);
            model.reviewCategory = $("#edit-model-review-category")[0].value.trim();
            model.approvalSubstitutions = splitField($("#edit-model-approval-substitutions")[0].value);
            
            return this.updateModelData(model);
        } else {
            return Promise.reject(null);
        }
    },
    
    editModelTestTrainingCategories : function () {
        var that = this;
        this.getEditModelSelectedCategories()
        .then (function (acceptedCategories) {
                if (acceptedCategories.length == 0) {
                    that.showBigMessageDialog("Included Training Categories", "No categories selected.");
                } else {
                    var message = acceptedCategories.sort().join("\n");
                    that.showBigMessageDialog("Included Training Categories", message);
                }
            });
    },
    
    editModelTestNormalizingSubstitutions : function () {
        var that = this;
        this.testSubstitutions([
                this.makeTestSubstitutions("edit-model-normalizing-substitutions")
            ], "Normalizing Substitutions");
    },
    
    editModelTestReviewSubstitutions : function () {
        var that = this;
        this.testSubstitutions([
                this.makeTestSubstitutions("edit-model-normalizing-substitutions"),
                this.makeTestSubstitutions("edit-model-review-substitutions")
            ], "Normalizing and Review Substitutions");
    },
    
    editModelTestApprovalSubstitutions : function () {
        var that = this;
        this.testSubstitutions([
                this.makeTestSubstitutions("edit-model-normalizing-substitutions"),
                this.makeTestSubstitutions("edit-model-review-substitutions"),
                this.makeTestSubstitutions("edit-model-approval-substitutions")
            ], "Normalizing, Review and Approval Substitutions");
    },
    
    testSubstitutions : function (substitutions, title) {
        var that = this;
        this.getEditModelSelectedCategories()
        .then (function (acceptedCategories) {
                if (acceptedCategories.length == 0) {
                    that.showBigMessageDialog(title, "No categories selected.");
                } else {
                    acceptedCategories = acceptedCategories.sort();
                    var message = "";
                    acceptedCategories.forEach(function (path) {
                            message = message + path;
                            substitutions.forEach(function (subst) {
                                    path = subst(path);
                                    message = message + " -> " + path;
                                });
                            message = message + "\n";
                        });
                    that.showBigMessageDialog(title, message);
                }
            });
    },
    
    getEditModelSelectedCategories : function () {
        var categorySearch = $("#edit-model-training-categories")[0].value.split(";");
        return this.getModelSelectedCategories (categorySearch);
    },
    
    updateModelData : function (data) {
        var that = this;
        return IMWS.post("v1/filesystem/file/writetext", {
                filename: that.config.userData + "\\models\\user\\" + data.model_dir_name + "\\model.json",
                data: JSON.stringify(data)
            });
    },
    
    trainCurrentModel : function () {
        var that = this;
        var modelId = that.getSelectedModel ();
        this.saveSelectedModel()
        .then (function  () {                
                if (modelId != null) {
                    var model = that.models[modelId];
                    return that.retrain(modelId, model.model_name, 
                        model.trainingCategories != null ? model.trainingCategories : [], 
                        model.numTrainingImages != null ? model.numTrainingImages : 1500,
                        model.numIterations != null ? model.numIterations : 4000);
                } else {
                    throw new Exception();
                }
            })
        .then (function () {
                return that.loadModels();
            })
        .then (function () {
                that.setSelectedModel(modelId);
                that.onModelsChange();
            });
    },
    
    retrain : function (modelDir, modelName, categorySearch, numFiles, numIterations) {
        var searches = [];
        var that = this;
        
        for (var i = 0; i < categorySearch.length; ++i) {
            searches.push(
                IMWS.get('v1/categories', {
                        fields: "id,path,files,childrencount,directfiles,directfilescount",
                        regexp: "path," + categorySearch[i]
                    })
            )
        }
        var requestFileName = that.config.tempDir + "\\" + Imception.APPNAME + ".json";
        
        var shuffle = function (array) {
            for (var i = array.length - 1; i > 0; i -= 1) {
                var j = Math.floor(Math.random() * (i + 1))
                var temp = array[i]
                array[i] = array[j]
                array[j] = temp
            }
            return array;
        }
        
        Promise.all([Promise.all(searches), this.resetJobControl()])
        .then (function (responses) {
                var request = {
                    baseUrl: IMatch.DEFAULT_URL,
                    "bin_dir": that.config.binDir,
                    "work_dir": that.config.tempDir,
                    "user_data_dir": that.config.userData,
                    "model_dir": modelDir,
                    "model_name": modelName,
                    "auth_token": that.authToken,
                    "job_control": that.jobControl,
                    "flags": {
                        "how_many_training_steps": numIterations
                    },
                    sets: {
                    }
                };
                
                var categoriesById = {};
                for (var i = 0; i < responses[0].length; ++i) {
                    var response = responses[0][i];
                    var categories = response.categories;
                    for (var j = 0; j < categories.length; ++j) {
                        var category = categories[j];
                        categoriesById[category.id] = category;
                    }
                }
                for (var cid in categoriesById) {
                    var category = categoriesById[cid];
                    if (category.childrenCount == 0) {
                        var files = category.files;
                        request.sets[category.path] = shuffle(files).slice(0, numFiles);
                    } else if (category.directFilesCount > 0) {
                        var files = category.directFiles;
                        request.sets[category.path] = shuffle(files).slice(0, numFiles);
                    }
                }
                return IMWS.post("v1/filesystem/file/writetext", {
                        filename: requestFileName,
                        data: JSON.stringify(request)
                    });
            })
        .then (function (response) {
                return IMatch.shellExecute({
                        executable : Imception.TF_SERVER,
                        verb: "open",
                        directory: that.config.binDir,
                        parameters : "retrain " + requestFileName,
                        showwindow : that.debugMode
                    })
                .then (function (response) {
                        if (response.result == "ok") {
                            return that.startProgress ("Training " + modelName);
                        } else {
                            console.log(response);
                            new IMatchModals().showErrorBox({
                                    headline: 'Unable to run TensorFlow',
                                    content: "The TensorFlow executable did not complete successfully. The error was: " + JSON.stringify(response)
                                });
                            throw new Exception ();
                        }
                    }, function () {
                    });
            })
        .then (function (response) {
                if (response.error != null && response.error == true) {
                    var m = new IMatchModals();
                    m.showErrorBox({
                            headline: "An error occurred when training " + modelName,
                            content: response.message
                        });
                } else {
                    return that.loadModels();
                }
            });
    },
    
    /*
    --------------------------------------------------------------------
    Create model dialog
    --------------------------------------------------------------------
    */
    
    
    createModel : function () {
        $("#create-dialog-name")[0].value = "";
        $("#create-dialog-folder")[0].value = "";
        $("#btn-create-dialog-ok")[0].disabled = true;
        $("#create-model-dialog").modal({});
    },
    
    createModelOk : function () {
        var modelName = $("#create-dialog-name")[0].value.trim ();
        var modelFolder = $("#create-dialog-folder")[0].value.trim ();
        if (modelName == "") {
            alert("No model name specified.");
            return;
        }
        if (modelFolder == "") {
            alert("No model folder specified.");
            return;
        }
        if (this.models[modelFolder] != null) {
            alert("A model already exists in that folder. Choose a different folder name.");
            return;
        }
        
        
        var modelInfo = {
            model_dir_name: modelFolder,
            model_name: modelName,
            architecture: "inception_v3"
        };
        var that = this;
        return IMWS.post("v1/filesystem/folder/create", {
                path: that.config.userData + "\\models\\user\\" + modelFolder
            })
        .then(function () {
                return that.updateModelData (modelInfo);
            })
        .then(function () {
                $("#create-model-dialog").modal("hide");
                return that.loadModels ();
            })
        .then(function () {
                that.setSelectedModel(modelFolder);
                that.onModelsChange();
            });
    },
    
    createModelCancel : function () {
        $("#create-model-dialog").modal("hide");
    },
    
    /*
    --------------------------------------------------------------------
    Big message dialog
    --------------------------------------------------------------------
    */
    
    showBigMessageDialog : function (title, message) {
        $("#big-message-dialog").modal({});
        $("#big-message-title")[0].innerText = title;
        $("#big-message-text")[0].value = message;
    },
    
    closeBigMessageDialog : function () {
        $("#big-message-dialog").modal("hide");
    },
    
    /*
    --------------------------------------------------------------------
    Job progress dialog
    --------------------------------------------------------------------
    */
    
    endProgress : function (title) {
        $("#progress-dialog").modal("hide");
    },
    
    startProgress : function (title) {
        $("#progress-dialog-title")[0].innerHTML = title;
        this.updateJobProgress ("", 0);
        $("#progress-dialog").modal({
                backdrop: "static"
            });
        var that = this;
        return new Promise(function (resolve, reject) {
                setTimeout (function () {
                        that.jobProgressTick (resolve, reject);
                    }, 1000);
            });
    },
    
    jobProgressTick : function (resolve, reject) {
        var that = this;
        this.getJobControl()
        .then(function (response) {
                that.updateJobProgress (response.message, response.progress);
                if (!response.completed) {
                    setTimeout (function () {
                            that.jobProgressTick (resolve, reject);
                        }, 1000);
                } else {
                    that.endProgress ();
                    resolve(response);
                }
            });
    },
    
    updateJobProgress : function (message, progressPct) {
        $("#progress-dialog-message")[0].innerHTML = "&#160;" + message + "&#160;";
        $("#progress-dialog-bar")[0].style.width = progressPct + "%";
        $("#progress-dialog-percent")[0].innerHTML = Math.round(progressPct) + "%";
    },
    
    cancelJob : function () {
        return this.setJobControl({
                cancel: true
            });
    },
    
    /*
    --------------------------------------------------------------------
    Job Control
    --------------------------------------------------------------------
    */
    
    resetJobControl : function () {
        return IMWS.post("v1/data", {
                name: Imception.APPNAME + ".job.read",
                value: JSON.stringify({
                        cancel: false
                    })
            })
        .then (function () {
                return IMWS.post("v1/data", {
                        name: Imception.APPNAME + ".job.write",
                        value: JSON.stringify({
                                message:"", 
                                progress: 0,
                                completed: false
                            })
                    });
            });
    },
    
    getJobControl : function () {
        return IMWS.get("v1/data", {
                name: Imception.APPNAME + ".job.write"
            }).then(function (response) {
                    return JSON.parse(response.value);
                });
    },
    
    setJobControl : function (jcData) {
        return IMWS.post("v1/data", {
                name: Imception.APPNAME + ".job.read",
                value: JSON.stringify(jcData)
            });
    },
    
    /*
    --------------------------------------------------------------------
    Options dialog
    --------------------------------------------------------------------
    */
    
    showOptions : function () {
        $("#option-dialog-bin-dir")[0].value = this.config.binDir;
        $("#option-dialog").modal({});
    },
    
    cancelOptions : function () {
        $("#option-dialog").modal("hide");
    },
    
    saveOptions : function () {
        var binDir = $("#option-dialog-bin-dir")[0].value.trim();
        var that = this;
        return IMatch.shellExecute({
                executable : Imception.TF_SERVER,
                verb: "open",
                directory: binDir,
                parameters : "version",
                showwindow : that.debugMode
            })
        .then (function (response) {
                if (response.result == "ok") {
                    that.config.binDir = binDir;
                    that.saveConfig();
                    $("#option-dialog").modal("hide");
                } else {
                    alert("Service did not start. Tried to start " + binDir + "\\" + Imception.TF_SERVER);
                }
            });
    },
    
    /*
    --------------------------------------------------------------------
    Model management
    --------------------------------------------------------------------
    */
    
    loadModels : function () {
        var that = this;
        return IMWS.get("v1/filesystem/folder/scan", {
                path: that.config.userData + "\\models\\user\\",
                filemask: ".*",
                regexp: true,
                recursive: true
            })
        .then(function (response) {
                var files = response.folders[0].subFolders;
                if (files != null && files.length > 0) {
                    var modelInfoPromises = [];
                    for (var i = 0; i < files.length; ++i) {
                        var pathElements = files[i].path.split("\\");
                        var modelDir = pathElements[pathElements.length - 2];
                        modelInfoPromises.push(
                            Promise.all([
                                    Promise.resolve(modelDir)
                                    ,
                                    IMWS.get("v1/filesystem/file/readtext", {
                                            filename: files[i].path + "model.json"
                                        }).then(function (response) {
                                                return JSON.parse(response.data);
                                            }, function (response) {
                                                return {};
                                            })
                                ])
                        );
                    }
                    return Promise.all(modelInfoPromises);
                } else {
                    return IMWS.post("v1/filesystem/folder/create", {
                            path: that.config.userData + "/models"
                        }).then(function(response) {
                                return [];
                            });
                }
            })
        .then(function (response) {
                that.models = {};
                for (var i = 0; i < response.length; ++i) {
                    var modelInfo = response[i];
                    that.models[modelInfo[0]] = modelInfo[1];
                }
                var cbModel = $("#cb-model")[0];
                cbModel.innerHTML = "";
                for (var k in that.models) {
                    var option = document.createElement("option");
                    option.innerText = that.models[k].model_name;
                    option.value = k;
                    cbModel.appendChild (option);
                }
            });
    },
    
    makeTestSubstitutions : function (elementId) {
        var values = $("#" + elementId)[0].value.split(";");
        return this.makeSubstitutions(values);
    },
    
    makeSubstitutions : function (values) {
        var result = [];
        values.forEach(function (v) {
                var parts = v.split(",");
                if (parts.length == 2) {
                    result.push([new RegExp (parts[0], "g"), parts[1]]);
                }
            });
        return function (input) {
            result.forEach(function (repl) {
                    input = input.replace(repl[0], repl[1]);
                });
            return input;
        };
    },
    
    getCategoryNameSubstitutions : function (model, preSubstitutions, substitutions) {
        var that = this;
        return this.getModelSelectedCategories(model.trainingCategories != null ? model.trainingCategories : [])
        .then (function (acceptedCategories) {
                return that.getCategoryNameSubstitutionsForCategories (acceptedCategories);
            });
    },
    
    getCategoryNameSubstitutionsForCategories : function (model, acceptedCategories, preSubstitutions, substitutions) {
        if (acceptedCategories.length == 0) {
            return {};
        } else {
            var result = {};
            acceptedCategories.forEach(function (path) {
                    preSubstitutions.forEach(function (subst) {
                            path = subst(path);
                        });
                    var original = path;
                    substitutions.forEach(function (subst) {
                            path = subst(path);
                        });
                    result[original] = path;
                });
            return result;
        }
    },
    
    /*
    --------------------------------------------------------------------
    Review Module
    --------------------------------------------------------------------
    */
    
    showReviewModule : function () {
        this.showModule("review");
        var model = this.models[this.getSelectedModel ()];
        $("#btn-review-apply-all").prop("disabled", model == null);
        var reviewPanelReject = $('#review-panel-reject')[0];
        var reviewPanelAccept = $('#review-panel-accept')[0];
        $('#review-image-multiple')[0].style.display = "none";
        reviewPanelReject.innerHTML = "";
        reviewPanelAccept.innerHTML = "";
        
        if (model == null) {
            $('#review-image')[0].style.display = "none";
            return;
        }
        var that = this;
        var reviewCategories = null;
        that.reviewResult = null;
        this.getCategoryNameSubstitutions (model, [
                this.makeSubstitutions(model.normalizingSubstitutions != null ? model.normalizingSubstitutions : []),
                this.makeSubstitutions(model.reviewSubstitutions != null ? model.reviewSubstitutions : [])
            ], [
                this.makeSubstitutions(model.approvalSubstitutions != null ? model.approvalSubstitutions : []),
            ])
        .then (function (rc) {
                reviewCategories = rc;
                return IMWS.get('v1/files/categories',{
                        idlist: IMatch.idlist.fileWindowSelection,
                        fields: "id,path,name"
                    });
            })
        .then(function(response) {
                if (response.files.length > 0) {
                    $('#review-image')[0].style.display = "";
                    $('#review-image')[0].src = "/v1/files/image?auth_token=&imagesize=SMALL&id=" + response.files[0].id;
                } else {
                    $('#review-image')[0].style.display = "none";
                }
                
                $("#btn-review-apply-all").prop("disabled", response.files.length == 0);
                $('#review-image-multiple')[0].style.display = response.files.length > 1 ? "block" : "none";
                
                var categories = {};
                that.reviewResult = {
                    actions : {},
                    files : response.files,
                    reviewCategories : reviewCategories
                };
                
                response.files.forEach(function(f) {
                        f.categories.forEach(function(c) {
                                if (categories[c.path] != null) {
                                    categories[c.path].count++;
                                }
                                else {
                                    categories[c.path] = { 
                                        category : c,
                                        count : 1
                                    }
                                }
                            });                        
                    });
                
                reviewPanelReject.innerHTML = "";
                reviewPanelAccept.innerHTML = "";
                
                for (var path in categories) {
                    if (reviewCategories[path] != null) {
                        var button = document.createElement("button");
                        button.type = "button";
                        button.className = "btn btn-default btn-sm btn-category" + (categories[path].count == response.files.length ? " btn-success" : "");
                        button.innerHTML = "<span>" + reviewCategories[path] + " (" + (categories[path].count) + ")</span>";
                        button.style.margin = "16px";
                        button.addEventListener("click", function (b, p) {
                                return function () {
                                    var fromPanel = b.parentElement;
                                    var toPanel = that.reviewResult.actions[path] ? reviewPanelReject : reviewPanelAccept;
                                    toPanel.appendChild (b);
                                    that.reviewResult.actions[path] = !that.reviewResult.actions[path];
                                }
                            }(button, path));
                        reviewPanelReject.appendChild (button);
                        that.reviewResult.actions[path] = false;
                    }
                }
            });
    },
    
    reviewApplyAll : function () {
        var that = this;
        if (that.reviewResult == null) {
            return;
        }
        var model = this.models[this.getSelectedModel ()];
        var categories = {};
        var allNames = "";
        for (var k in this.reviewResult.actions) {
            if (this.reviewResult.reviewCategories[k] != null) {
                categories[k] = {
                    action: this.reviewResult.actions[k],
                    fileIds: [],
                    reviewPath: k,
                    approvedPath: this.reviewResult.reviewCategories[k]
                };
            }
        }
        var reviewCategory = null;
        if (model.reviewCategory != null && model.reviewCategory != "") {
            reviewCategory = model.reviewCategory;
            categories[reviewCategory] = {
                action: false,
                fileIds: [],
                reviewPath: reviewCategory
            };
        }
        this.reviewResult.files.forEach (function (f) {
                f.categories.forEach(function(c) {
                        if (categories[c.path] != null) {
                            categories[c.path].fileIds.push(f.id);
                        }
                    });
                if (reviewCategory != null) {
                    categories[reviewCategory].fileIds.push(f.id);
                }
            });
        for (var k in categories) {
            if (categories[k].fileIds.length > 0 && categories[k].action) {
                if (allNames.length > 0) {
                    allNames = allNames + ",";
                }
                allNames = allNames + categories[k].approvedPath;
            }
        }
        var assignmentCalls = [];
        
        var createCategories = null;
        if (allNames != "") {
            createCategories = IMWS.post('v1/categories/create',{
                    parentid: 0,
                    name: allNames
                });
        } else {
            createCategories = Promise.resolve(null);
        }
        
        that.reviewResult = null;
        
        return createCategories.then (function () {
                var assignmentCalls = [];
                for (var k in categories) {
                    if (categories[k].action) {
                        assignmentCalls.push (
                            IMWS.post('v1/categories/assign',{
                                    path: categories[k].approvedPath,
                                    fileid: categories[k].fileIds.join(",")
                                })
                        );
                    }
                    assignmentCalls.push (
                        IMWS.post('v1/categories/unassign',{
                                path: categories[k].reviewPath,
                                fileid: categories[k].fileIds.join(",")
                            })
                    );
                }                
                return Promise.all(assignmentCalls);
            })
        .then (function () {
                that.showReviewModule ();
            });
    },
    
    getModelSelectedCategories : function (categorySearch) {
        var searches = [];
        var that = this;
        
        for (var i = 0; i < categorySearch.length; ++i) {
            var searchExpr = categorySearch[i].trim();
            if (searchExpr != "") {
                searches.push(
                    IMWS.get('v1/categories', {
                            fields: "id,path,childrencount,directfilescount",
                            regexp: "path," + searchExpr
                        })
                )
            }
        }
        if (searches.length == 0) {
            return Promise.resolve([]);            
        } else {
            return Promise.all(searches)
            .then (function (responses) {
                    var categoriesById = {};
                    for (var i = 0; i < responses.length; ++i) {
                        var response = responses[i];
                        var categories = response.categories;
                        for (var j = 0; j < categories.length; ++j) {
                            var category = categories[j];
                            categoriesById[category.id] = category;
                        }
                    }
                    var acceptedCategories = [];
                    for (var cid in categoriesById) {
                        var category = categoriesById[cid];
                        if (category.childrenCount == 0) {
                            acceptedCategories.push(category.path);
                        } else if (category.directFilesCount > 0) {
                            acceptedCategories.push(category.path);
                        }
                    }
                    
                    return acceptedCategories;                    
                });
        }
    },
    
    
    onModelsChange : function () {
        if (this.currentModule == "models") {
            this.showModelsModule();
        } else if (this.currentModule == "review") {
            this.showReviewModule();
        } else if (this.currentModule == "label") {
            this.showLabelModule();
        }
    },
    
    onSelectionChanged : function () {
        if (this.currentModule == "label") {
            this.showLabelModule();
        } else if (this.currentModule == "review") {
            this.showReviewModule();
        }
    },
    
    initialize : function () {
        var that = this;
        this.dialogActive = false;
        this.debugMode = !IMatch.isEmbedded();
        
        $(".btn").on("click", function (e) {
                e.currentTarget.blur ();
            });
        
        $("#btn-module-label").on("click", function (e) {
                that.showLabelModule ();
            });
        
        $("#btn-module-review").on("click", function (e) {
                that.showReviewModule ();
            });
        
        $("#btn-module-models").on("click", function (e) {
                that.showModelsModule ();
            });
        
        $("#btn-label-single").on("click", function (e) {
                that.labelSingle ();
            });
        
        $("#btn-label-selected").on("click", function (e) {
                that.labelSelected ();
            });
        
        $("#btn-review-apply-all").on("click", function (e) {
                that.reviewApplyAll ();
            });
        
        $("#btn-edit-model-train").on("click", function (e) {
                that.trainCurrentModel ();
            });
        
        $("#btn-big-message-ok").on("click", function (e) {
                that.closeBigMessageDialog ();
            });
        
        $("#btn-edit-model-test-training-categories").on("click", function (e) {
                that.editModelTestTrainingCategories ();
            });        
        
        $("#btn-edit-model-test-normalizing-substitutions").on("click", function (e) {
                that.editModelTestNormalizingSubstitutions ();
            });
        
        $("#btn-edit-model-test-review-substitutions").on("click", function (e) {
                that.editModelTestReviewSubstitutions ();
            });
        
        $("#btn-edit-model-test-approval-substitutions").on("click", function (e) {
                that.editModelTestApprovalSubstitutions ();
            });
        
        $("#btn-edit-model-save").on("click", function (e) {
                that.saveSelectedModel ();
            });
        
        $("#btn-edit-model-create").on("click", function (e) {
                that.createModel ();
            });
        
        $("#btn-edit-model-manage").on("click", function (e) {
                IMatch.shellExecute({
                        executable : "explorer.exe",
                        verb : "open",
                        parameters : that.config.userData + "\\models\\user\\",
                        showwindow : true
                    });
            });
        
        $("#btn-create-dialog-ok").on("click", function (e) {
                that.createModelOk ();
            });
        
        $("#btn-create-dialog-cancel").on("click", function (e) {
                that.createModelCancel ();
            });
        
        $("#cb-model").on("change", function (e) {
                that.onModelsChange ();
            });
        
        $("#create-dialog-name").on("change keypress keyup blur", function (e) {
                var modelName = $("#create-dialog-name")[0].value;
                modelName = modelName.toLowerCase ().replace(/\s+/g, "_").replace(/\W/g, "").trim();
                $("#create-dialog-folder")[0].value = modelName;
                $("#btn-create-dialog-ok")[0].disabled = modelName == "";
            });
        
        $("#create-dialog-folder").on("change keypress keyup blur", function (e) {
                $("#btn-create-dialog-ok")[0].disabled = $("#create-dialog-folder")[0].value.trim() == "";
            });
        
        $("#btn-progress-cancel").on("click", function (e) {
                that.cancelJob ()
                .then(function () {
                        that.endProgress ();
                    });
            });
        
        $("#btn-options").on("click", function (e) {
                that.showOptions ();
            });
        
        $("#btn-options-cancel").on("click", function (e) {
                that.cancelOptions ();
            });
        
        $("#btn-options-ok").on("click", function (e) {
                that.saveOptions ();
            });
        
        $("#btn-reload-models").on("click", function (e) {
                that.loadModels().then (function () {
                        that.showLabelModule ();
                    });
            });
        
        this.config = {
            version: "1.0",
            binDir: ""
        };
        this.models = {};
        IMWS.get('v1/data',{
                name : Imception.APPNAME,
            })
        .then(function(response) {
                var data = JSON.parse(response.value);
                if (data.version == "1.0") {
                    that.config = data;
                }
                return IMWS.get("v1/filesystem/specialfolders");
            }, function(response) {
                return IMWS.get("v1/filesystem/specialfolders");
            })
        .then(function(response) {
                that.config.tempDir = response.folders.TEMP;
                that.config.commonAppData = response.folders.COMMON_APPDATA;
                that.config.appData = response.folders.APPDATA;
                that.config.userData = that.config.appData + Imception.APPNAME;
                return that.loadModels();
            })
        .then(function (response) {
                that.configure ();
            })
        .catch(function (error) {
                that.configure ();
            }); 
    },
    
    saveConfig : function () {
        return IMWS.post('v1/data',{
                name : Imception.APPNAME,
                value: JSON.stringify(this.config)
            });
    },
    
    configure : function () {
        var that = this;
        this.authToken = IMWS.config.auth_token;
        this.jobControl = {
            "read": IMatch.DEFAULT_URL + "/v1/data?name=" + encodeURIComponent(Imception.APPNAME + ".job.read") + "&auth_token=" + encodeURIComponent(this.authToken),
            "write": {
                "url": IMatch.DEFAULT_URL + "/v1/data",
                "parameters": {
                    "name" : Imception.APPNAME + ".job.write",
                    "auth_token": this.authToken
                }
            }
        }
        
        var that = this;
        if (IMatch.isEmbedded()) {
            IMatch.openWebSocket();
            
            IMatch.webSocket().onmessage = function(e) {
                var msg = JSON.parse(e.data);
                
                if (msg.msg == "fileWindow.focusChanged") {
                    var fileId = msg.data.id;
                    that.onSelectionChanged (fileId);
                } else if (msg.msg == "fileWindow.selectionChanged") {
                    that.onSelectionChanged ();
                }
            };
        }
        
        
        
        if (this.config.binDir == "") {
            this.showModelsModule ()
            .then(function() {
                    that.showOptions();
                });
        } else if (Object.keys(this.models).length == 0) {
            this.showModelsModule ()
            .then(function() {
                    that.createModel();
                });
        } else {
            this.showLabelModule ();
        }
    },
    
    showModule : function (id) {
        var modules = ["label", "review", "models"];
        modules.forEach(function (m) {
                $("#module-" + m)[0].style.display = "";
            });
        $("#module-" + id)[0].style.display = "block";
        this.currentModule = id;
    }
}