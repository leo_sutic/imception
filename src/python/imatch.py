# Copyright 2018 Leo Sutic <leo@sutic.nu>. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import requests
import json

class JobCanceledException(BaseException):
    """
    Raised when a job has been canceled.
    """
    pass

class JobControl(object):
    """
    Interface to job control in IMWS.
    """

    def __init__(self, job_control_parameters):
        """
        Parameters:
            job_control_parameters(dict,optional): if not ``None``, should have
                the keys ``read`` for the URL to GET in order to get job cancellation
                status, ``write`` for a dict with a ``url`` to POST job status to and
                a ``parameters`` dict with post parameters like ``auth_token``.
                If none, disables job control (no data is sent, and the job is never
                canceled).
        """
        if job_control_parameters is not None:
            self.read_url = job_control_parameters["read"]
            self.write_url = job_control_parameters["write"]["url"]
            self.write_parameters = job_control_parameters["write"]["parameters"]
        else:
            self.read_url = None
            self.write_url = None
            self.write_parameters = None
        self.low = 0
        self.high = 100
        self.last_progress = 0
        self.last_message = 0
        self.is_completed = False
        self.is_error = False
        
    def set_scale(self, low, high):
        """
        Adds internal scaling of the progress value. Calls to :meth:`~imatch.JobControl.update`
        with ``progress`` set to zero will result in ``low`` being reported to IMWS,
        ``progress`` set to 100 will result in ``high`` being reported to IMWS.
        This enabled a client to split a big job into several parts and have each part
        report its progress from 0 to 100.
        
        Parameters:
            low (int): the new low value
            high (int): the new high value
        """
        self.low = low
        self.high = high
        
    def is_canceled(self):
        """
        Tests whether the job has been canceled.
        
        Returns:
            bool: ``True``  iff the job has been canceled.
        """
        if self.read_url is not None:
            data = requests.get(self.read_url).json()
            return json.loads(data["value"]).get("cancel", False)
        else:
            return False
            
    def check_cancel(self):
        """
        Checks whether the job has been canceled, and if so, raises
        an :exc:`~imatch.JobCanceledException`.
        """
        if self.is_canceled():
            raise JobCanceledException()
        
    def update(self, progress, message="", completed=False, error=False):
        """
        Update and send job status.
        """
        self.last_progress = self.low + (progress * (self.high - self.low) / 100)
        self.last_message = message
        self.is_completed = completed
        self.is_error = error
        self._send()
        
    def update_message(self, message):
        """
        Only update the message (leave progress and completion as-is)
        and send data to IMWS.
        """
        self.last_message = message
        self._send()
        
    def complete(self):
        """
        Mark this job as having completed and send the data
        to IMWS.
        """
        self.is_completed = True
        self.last_progress = 100
        self._send()
        
    def _send(self):
        """
        Send current job status to IMWS.
        """
        if self.write_url is not None:
            params = {}
            params.update(self.write_parameters)
            params["value"] = json.dumps({
                "progress": self.last_progress,
                "message": self.last_message,
                "completed" : self.is_completed,
                "error" : self.is_error
            })
            requests.post(self.write_url, data=params)

