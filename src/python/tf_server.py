# Copyright 2018 Leo Sutic <leo@sutic.nu>. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Main module for the ``tf_server``  application. Dispatches to 
``retrain.py``  or ``label_image``  depending on the first command-line
parameter.
"""

import sys
import retrain
import label_image
import time
from imatch import JobCanceledException

if __name__ == "__main__":
    module = sys.argv[1]
    del sys.argv[1]
    try:
        if module == "retrain":
            retrain.run_from_launcher()
        elif module == "label_image":
            label_image.run_from_launcher()
        elif module == "version":
            print("1.0")
        else:
            raise Exception("Unknown module {}".format(module))
    except Exception as e:
        # Sleep for a minute so we can see the stack trace
        import traceback
        traceback.print_exc()
        time.sleep(60)
        