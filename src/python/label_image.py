# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Use a model to detect labels for an image.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import json
import os
import uuid
from six.moves import urllib

import numpy as np
import tensorflow as tf
from imatch import JobControl

JOB_CONTROL = None
REQUEST = None

def load_graph(model_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()
    
    with open(model_file, "rb") as f:
        graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)
    
    return graph

def read_tensor_from_image_file(file_name, input_height=299, input_width=299,
                input_mean=0, input_std=255):
    input_name = "file_reader"
    output_name = "normalized"
    file_reader = tf.read_file(file_name, input_name)
    image_reader = tf.image.decode_jpeg(file_reader, channels = 3,
                                        name='jpeg_reader')
    float_caster = tf.cast(image_reader, tf.float32)
    dims_expander = tf.expand_dims(float_caster, 0);
    resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
    normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])
    sess = tf.Session()
    result = sess.run(normalized)
    
    return result

def load_labels(label_file):
    label = []
    proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
    for l in proto_as_ascii_lines:
        label.append(l.rstrip())
    return label

def download_image(base_url, file_id, dest_dir):
    url = "{}/v1/files/image?auth_token=&imagesize=SMALL&id={}".format(base_url, file_id)
    print(url)
    base_name = "{}.jpg".format(uuid.uuid4().hex)
    file_name = os.path.join(dest_dir, base_name)
    urllib.request.urlretrieve(url, file_name)
    return file_name

def main():
    global REQUEST
    global JOB_CONTROL
    
    user_data_dir = REQUEST["user_data_dir"]
    model_dir_name = REQUEST["model_dir"]
    work_dir = REQUEST["work_dir"]
    base_url = REQUEST["baseUrl"]
    model_dir = os.path.join(user_data_dir, "models", "user", model_dir_name)
    
    with open(os.path.join(model_dir, "model.json"), "rb") as f:
        model_info = json.loads(f.read().decode("utf-8"))
    
    model_file = os.path.join(model_dir, model_info["graph_file_name"])
    label_file = os.path.join(model_dir, "labels.txt")
    input_height = model_info["input_height"]
    input_width = model_info["input_width"]
    input_mean = model_info["input_mean"]
    input_std = model_info["input_std"]
    input_layer = model_info["resized_input_tensor_name"].split(":")[0]
    input_tensor_index = int(model_info["resized_input_tensor_name"].split(":")[1])
    output_layer = model_info["final_tensor_name"]
    
    num_top = REQUEST.get("top", -1)
    min_confidence = REQUEST.get("min_confidence", -1)

    JOB_CONTROL.update(0, "Initializing...")

    graph = load_graph(model_file)
    labels = load_labels(label_file)
    
    input_name = "import/" + input_layer
    output_name = "import/" + output_layer
    input_operation = graph.get_operation_by_name(input_name);
    output_operation = graph.get_operation_by_name(output_name);
        
    result = {}
    num_images = len(REQUEST["images"])
    images_done = 1
    with tf.Session(graph=graph) as sess:
        for file_id in REQUEST["images"]:
            JOB_CONTROL.check_cancel()
            JOB_CONTROL.update(100 * images_done / num_images, "Labeling image {} of {}".format(images_done, num_images))
            result[file_id] = {}
            file_name = download_image(base_url, file_id, work_dir)
            t = read_tensor_from_image_file(file_name,
                                            input_height=input_height,
                                            input_width=input_width,
                                            input_mean=input_mean,
                                            input_std=input_std)
        
            results = sess.run(output_operation.outputs[0],
                               {input_operation.outputs[input_tensor_index]: t})
            results = np.squeeze(results)
        
            filtered_results = results.argsort()
            if num_top > 0:
                filtered_results = filtered_results[-num_top:]
            
            filtered_results = filtered_results[::-1]
            
            for i in filtered_results:
                confidence = float(results[i])
                if confidence > min_confidence:
                    result[file_id][labels[i]] = confidence
            os.remove(file_name)
            images_done += 1
            
    
    with open(REQUEST["output_file"], "wb") as f:
        f.write(json.dumps(result).encode("utf-8"))

def run_from_launcher():
    global REQUEST
    global JOB_CONTROL
    
    request_file = sys.argv[1]
    with open(request_file, "rb") as f:
        REQUEST = json.loads(f.read().decode("utf-8"))
        
    JOB_CONTROL = JobControl(REQUEST["job_control"])
        
    JOB_CONTROL.update(0, "Starting...")
    try:
        main()
    except Exception as e:
        JOB_CONTROL.update(100, str(e), True, error=True)
        raise
    finally:
        JOB_CONTROL.complete()
